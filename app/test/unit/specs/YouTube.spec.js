import Vue from 'vue';
import YouTube from '@/components/YouTube';

describe('YouTube.vue', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(YouTube);
    const vm = new Constructor().$mount();
    expect(vm.$el.querySelector('.YouTube h1').textContent)
      .to.equal('Welcome to Your Vue.js App');
  });
});
